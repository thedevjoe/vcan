let globals = {
    x509: {
        cert_text: null,
        cert: null,
        pkey_text: null,
        pkey: null
    },
    socket: null,
    known_clients: {},
    operations: {
        DISCOVER: require('./ops/discover'),
        CERT_REQUEST: require('./ops/certRequest'),
        CERT_ANSWER: require('./ops/certAnswer'),
        PING: require('./ops/ping'),
        PONG: require('./ops/pong'),
        APPS_LIST_REQUEST: require('./ops/appsListRequest'),
        GENERIC_RESPONSE: require('./ops/genericResponse'),
        UNEN_RESPONSE: require('./ops/unenResponse'),
        APP_DASH: require('./ops/appDash'),
        TRIGGER_HOOK: require('./ops/triggerHook'),
        C_FIND: require('./ops/cFind')
    },
    upSince: 0,
    apps: {},
    config: null,
    resolveName: (addr) => {
        const gl = require('./globals');
        if(gl.known_clients[addr] === undefined) return addr;
        let cl = gl.known_clients[addr];
        if(cl.x509 == null) return addr;
        return addr + "/" + cl.x509.subject.commonName;
    },
    staleConnections: (staleAt = 60000*4) => {
        const gl = require('./globals');
        let stale = {};
        Object.keys(gl.known_clients).forEach(e => {
            let obj = gl.known_clients[e];
            if(!obj.pingSent && obj.lastContact < Date.now()-(staleAt)) {
                stale[e] = obj;
            }
        });
        return stale;
    },
    activeConnections: (staleAt = 60000*4) => {
        const gl = require('./globals');
        let stale = {};
        Object.keys(gl.known_clients).forEach(e => {
            let obj = gl.known_clients[e];
            if(!obj.pingSent && obj.lastContact > Date.now()-(staleAt)) {
                stale[e] = obj;
            }
        });
        return stale;
    },
    getUDPBindAddress: () => {
        const os = require('os');
        const {err} = require('./debug');
        if(globals.config.interface == null)
            return "0.0.0.0";
        let interfaces = os.networkInterfaces();
        console.log(Object.keys(interfaces));
        let i = interfaces[globals.config.interface];
        if(i == null) {
            err("Unable to resolve a bind address for UDP. Using 0.0.0.0");
            return "0.0.0.0";
        }
        i = i.find(e => {return e.family === "IPv4";});
        if(i == null) {
            err("Unable to resolve a bind address for UDP. Using 0.0.0.0");
            return "0.0.0.0";
        }
        return i.address;
    },
    getBroadcastAddress: () => {
        const broadcastA = require('broadcast-address');
        if(globals.config.interface == null)
            return "255.255.255.255";
        const os = require('os');
        let interfaces = os.networkInterfaces();
        let i = interfaces[globals.config.interface];
        const {err} = require('./debug');
        if(i == null) {
            err("Unable to resolve a broadcast address for UDP. Using 255.255.255.255");
            return "255.255.255.255";
        }
        return broadcastA(globals.config.interface);
    }
};

module.exports = globals;