let debug = true;

module.exports = {
    log: (m) => {
        if(debug)
        console.log("[D] " + m);
    },
    err: (m) => {
        if(debug)
        console.error("[D] " + m);
    }
}