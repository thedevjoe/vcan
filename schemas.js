const {Validator} = require('jsonschema');

let v = new Validator();

let webhookSchema = {
    id: "/applicationWebhook",
    type: "object",
    properties: {
        id: {type: "string", pattern: /[a-z0-9-_]{3,50}/},
        label: {type: "string"},
        type: {type: "string", pattern: /[a-z0-9-_]{3,50}/},
        url: {type: "string", format: "uri"}
    },
    required: ["id", "label", "type", "url"]
}

let appSchema = {
    id: "/applicationBase",
    type: "object",
    properties: {
        name: {type: "string", pattern: /[a-z0-9-_]{3,50}/},
        webhooks: {type: "array", items: {"$ref": "/applicationWebhook"}},
        dashUri: {type: "uri"},
        ignoreSSL: {type: "bool"},
        serviceId: {type: "string", pattern: /[a-z0-9-_]{3,50}/}
    },
    required: ["name"]
}

let packet = {
    id: "/pack",
    type: "object",
    properties: {
        s: {type: "string"},
        d: {type: "string"}
    },
    required: ["s", "d"]
};

let data = {
    id: "/pack/data",
    type: "object",
    properties: {
        "_c": {"$ref": "/pack/data/comb"},
        op: {type: "string"},
        data: {oneof: [{type: "object"}, {type: "null"}]},
        nonce: {type: "number"},
        seq: {type: "number"}
    },
    required: ["op", "seq"]
}

let comb = {
    id: "/pack/data/comb",
    type: "object",
    properties: {
        num: {type: "number"},
        of: {type: "number"}
    },
    required: ["num", "of"]
}

v.addSchema(webhookSchema);
v.addSchema(appSchema);

v.addSchema(comb);
v.addSchema(data);
v.addSchema(packet);
module.exports = v;