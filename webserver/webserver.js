const express = require('express');
const bodyParser = require('body-parser');
let globals = require('../globals');
let {log} = require('../debug');
let app = express();
const functions = require('../functions');
const val = require('../schemas');

globals.upSince = Date.now();

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.json({vCAN: "1.0", "nodeName": globals.x509.cert.subject.commonName, "serial": globals.x509.cert.serialNumber, "certificate": globals.x509.cert_text});
});

app.get('/services/:srv', (req, res) => {
    let f = req.params.srv;
    functions.getNodeServices(f).then(e => {
        res.json(e);
    });
})

app.get('/nodes', (req, res) => {
    let a = globals.activeConnections();
    let out = Object.keys(a).map(e => {
        let o = a[e];
        return {"nodeName": o.x509.subject.commonName, "serial": o.x509.serialNumber, "lastContact": o.lastContact, "address": e};
    });
    res.json(out);
});

app.get('/n/local/apps', (req, res) => {
    res.json({apps: functions.getApps()});
});

app.get('/n/local/apps/:name/dash', (req, res) => {
    functions.getAppDash(req.params.name).then(e => {
        res.json({dash: e});
    });
});

app.post('/n/local/apps/:name/hook/:hook', (req, res) => {
    functions.triggerAppHook(req.params.name, req.params.hook, req.body).then(e => {
        res.json({response: e});
    });
});

app.get('/n/:serial/apps/', (req, res) => {
    let node = functions.getNode(req.params.serial);
    if(node == null) return res.sendStatus(404);
    functions.getNodeApps(node).then(e => {
        res.json(e);
    });
});

app.get('/n/:serial/apps/:name/dash', (req, res) => {
    let node = functions.getNode(req.params.serial);
    if(node == null) return res.sendStatus(404);
    functions.getNodeAppDash(node, req.params.name).then(e => {
        res.json(e);
    });
});

app.post('/n/:serial/apps/:name/hook/:hook', (req, res) => {
    let node = functions.getNode(req.params.serial);
    if(node == null) return res.sendStatus(404);
    functions.triggerNodeHook(node, req.params.name, req.params.hook, req.body).then(e => {
        res.json(e);
    });
});

app.post('/register', (req, res) => {
    let v = val.validate(req.body, {"$ref": "/applicationBase"});
    if(!v.valid) return res.status(400).json({"errors": v.errors.map(e => e.property)});
    globals.apps[req.body.name] = req.body;
    res.sendStatus(204);
})

app.listen(globals.config.webPort, '127.0.0.1', () => {
    require('../apps');
});
log("Webserver UP.");