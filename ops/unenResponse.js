module.exports = {
    verifySignature: true,
    invoke: (data, inf, nonce) => {
        const {log} = require('../debug');
        let {responseQueue} = require('./genericResponse');
        if(nonce != null && responseQueue[nonce] !== undefined) {
            responseQueue[nonce].callback(data);
            delete responseQueue[nonce];
        }
    },
}