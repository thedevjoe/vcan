module.exports = {
    verifySignature: false,
    invoke: (data, inf, nonce) => {
        const tr = require('../transport');
        const {opCode} = require('../enums');
        const {log} = require('../debug');
        const globals = require('../globals');
        const functions = require('../functions');
        if(data.name == null || typeof(data.name) !== 'string') return;
        functions.getAppDash(data.name).then(e => {
            tr.sendData({op: opCode.GENERIC_RESPONSE, data: {dash: e}, nonce}, inf.address, true);
            log("Sent GENERIC_RESPONSE to APP_DASH");
        });
    }
}