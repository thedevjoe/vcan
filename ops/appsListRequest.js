module.exports = {
    verifySignature: false,
    invoke: (data, inf, nonce) => {
        const tr = require('../transport');
        const {opCode} = require('../enums');
        const {log} = require('../debug');
        const globals = require('../globals');
        const functions = require('../functions');
        tr.sendData({op: opCode.GENERIC_RESPONSE, data: {apps: functions.getApps()}, nonce}, inf.address, true);
        log("Sent GENERIC_RESPONSE to APPS_LIST_REQUEST");
    }
}