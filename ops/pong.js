module.exports = {
    verifySignature: true,
    invoke: (data, inf, nonce) => {
        const {log} = require('../debug');
        let {pongQueue} = require('./pong');
        if(nonce != null && pongQueue[nonce] !== undefined) {
            pongQueue[nonce]();
            delete pongQueue[nonce];
        }
        log("Ping/Pong Success!");
    },
    pongQueue: {}
}