module.exports = {
    verifySignature: false,
    invoke: (data, inf, nonce) => {
        const {Certificate} = require('@fidm/x509');
        const helpers = require('../helpers');
        const globals = require('../globals');
        if(data.x509 === undefined || typeof(data.x509) !== 'string') return;
        let cert = null;
        try {
            cert = Certificate.fromPEM(data.x509);
        } catch(e) {
            return;
        }
        if(!helpers.registerNode(inf.address, cert)) return;
        const {log} = require('../debug');
        log(inf.address + " identified as " + cert.subject.commonName);
        const tr = require('../transport');
        const {opCode} = require('../enums');
        tr.sendData({op: opCode.CERT_ANSWER, data: {x509: globals.x509.cert_text}}, inf.address);
        log("Sent CERT_ANSWER to CERT_REQUEST");
    }
}