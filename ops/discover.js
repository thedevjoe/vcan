module.exports = {
    verifySignature: false,
    invoke: (data, inf, nonce) => {
        const tr = require('../transport');
        const {opCode} = require('../enums');
        const {log} = require('../debug');
        const globals = require('../globals');
        tr.sendData({op: opCode.CERT_REQUEST, data: {x509: globals.x509.cert_text}}, inf.address);
        log("Sent CERT_REQUEST to DISCOVER");
    }
}