module.exports = {
    verifySignature: true,
    decryptData: false,
    invoke: (data, inf, nonce) => {
        const globals = require('../globals');
        const transport = require('../transport');
        const schema = require('jsonschema');
        const functions = require('../functions');
        const enums = require('../enums');
        let f = schema.validate(data, {type: "object", properties: {sid: {type: "string", pattern: /[a-z0-9-_]{5,50}/}}, required: ['sid']});
        if(!f.valid) return;
        let resp = [];
        Object.keys(globals.apps).forEach(name => {
            let app = globals.apps[name];
            if(app.serviceId == null) return;
            if(app.serviceId === data.sid) resp.push(functions.appToPubJson(name));
        })
        if(resp.length > 0)
            transport.sendData({op: enums.opCode.GENERIC_RESPONSE, data: {apps: resp}, nonce: nonce}, inf.address, true);
    }
}