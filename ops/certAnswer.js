module.exports = {
    verifySignature: false,
    invoke: (data, inf, nonce) => {
        const {Certificate} = require('@fidm/x509');
        const helpers = require('../helpers');
        if(data.x509 === undefined || typeof(data.x509) !== 'string') return;
        let cert = null;
        try {
            cert = Certificate.fromPEM(data.x509);
        } catch(e) {
            return;
        }
        if(!helpers.registerNode(inf.address, cert)) return;
        const {log} = require('../debug');
        log(inf.address + " identified as " + cert.subject.commonName);
        const tr = require('../transport');
        const {opCode} = require('../enums');
        tr.sendData({op: opCode.PING, data: null, nonce: Math.round(Math.random()*1000)}, inf.address);
        log("Certificate Handshake Complete.");
    }
}