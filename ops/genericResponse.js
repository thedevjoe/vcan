module.exports = {
    verifySignature: true,
    decryptData: true,
    invoke: (data, inf, nonce) => {
        const {log} = require('../debug');
        let {responseQueue} = require('./genericResponse');
        if(nonce != null && responseQueue[nonce] !== undefined) {
            responseQueue[nonce].callback(data, inf);
            if(!responseQueue[nonce].expectBatch) // There is expected to be multiple responses, so don't delete the callback
                delete responseQueue[nonce];
        }
    },
    responseQueue: {},
    responseBuilder: {}
}