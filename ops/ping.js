module.exports = {
    verifySignature: true,
    invoke: (data, inf, nonce) => {
        const tr = require('../transport');
        const {opCode} = require('../enums');
        const {log} = require('../debug');
        tr.sendData({op: opCode.PONG, data: null, nonce}, inf.address);
        log("Sent PONG to PING");
    }
}