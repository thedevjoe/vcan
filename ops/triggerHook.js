module.exports = {
    verifySignature: false,
    invoke: (data, inf, nonce) => {
        const tr = require('../transport');
        const {opCode} = require('../enums');
        const {log} = require('../debug');
        const globals = require('../globals');
        const functions = require('../functions');
        if(data.name == null || typeof(data.name) !== 'string') return;
        if(data.webhook == null || typeof(data.webhook) !== 'string') return;
        if(data.data == null || typeof(data.data) !== 'object') return;
        functions.triggerAppHook(data.name, data.webhook, data.data, inf).then(e => {
            tr.sendData({op: opCode.GENERIC_RESPONSE, data: {response: e}, nonce}, inf.address, true);
            log("Sent GENERIC_RESPONSE to TRIGGER_HOOK");
        });
    }
}