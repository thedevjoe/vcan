const axios = require('axios');
const os = require('os');
const fs = require('fs');
let app = (require('express'))();
const path = require('path');
const {log} = require('../../debug');
const globals = require('../../globals');
const { exec } = require("child_process");
 // 22647

let pending = null;

app.get('/panel', (req, res) => {
    res.send(fs.readFileSync(path.join(__dirname, 'index.html')));
});

app.post('/inf', (req, res) => {
    res.json({"name": globals.x509.cert.subject.commonName, totalNodes: Object.keys(globals.known_clients).length,
    regApps: Object.keys(globals.apps).length, upSince: globals.upSince});
});

app.post('/prc', (req, res) => {
    res.json({freemem: os.freemem(), totalmem: os.totalmem(), load: os.loadavg(), pendingAction: pending});
});

app.post('/shutdown', (req, res) => {
    if(pending != null) return res.sendStatus(400);
    switch(os.platform()) {
        case "win32":
            exec("shutdown /s /t 60 /d u:0:0 /c \"Shutdown request via vCAN\"");
            break;
        case "linux":
            exec("shutdown -P +1 \"Shutdown request via vCAN\"");
            break;
    }
    pending = "shutdown";
    res.sendStatus(204);
});

app.post('/restart', (req, res) => {
    if(pending != null) return res.sendStatus(400);
    switch(os.platform()) {
        case "win32":
            exec("shutdown /r /t 60 /d u:0:0 /c \"Restart request via vCAN\"");
            break;
        case "linux":
            exec("shutdown -r +1 \"Restart request via vCAN\"");
            break;
    }
    pending = "restart";
    res.sendStatus(204);
});

app.post('/update', (req, res) => {
    exec("git pull origin master");
    res.sendStatus(204);
});

app.listen(22647, () => {
    log("System Service Running");
});

function register() {
    axios.post('http://localhost:22646/register', JSON.stringify({
        name: "system",
        dashUri: "http://localhost:22647/panel",
        webhooks: [
            {
                id: "basic_info",
                label: "Basic Information Query",
                type: "client",
                url: "http://localhost:22647/inf"
            },
            {
                id: "prc",
                label: "OS Statistics",
                type: "client",
                url: "http://localhost:22647/prc"
            },
            {
                id: "shutdown",
                label: "Shutdown",
                type: "shutdown",
                url: "http://localhost:22647/shutdown"
            },
            {
                id: "restart",
                label: "Restart",
                type: "restart",
                url: "http://localhost:22647/restart"
            },
            {
                id: "update",
                label: "Update vCAN",
                type: "update",
                url: "http://localhost:22647/update"
            }
        ]
    }), {
        headers: {
            "Content-Type": "application/json"
        }
    });
}

register();