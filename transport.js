let globals = require('./globals');
let {log, err} = require('./debug');
const crypto = require('crypto');
const helpers = require('./helpers');

const MAX_SIZE = 150;

let trs = {
    sign: (data) => {
        return globals.x509.pkey.sign(data, "sha256");
    },
    verify: (data, signature, cert) => {
        return cert.publicKey.verify(data, signature, 'sha256');
    },
    buildPacket: ({op, data, nonce}, {encrypt, pubKey} = {encrypt: false}) => {
        if(encrypt) {
            let d = [];
            let json = JSON.stringify(data);
            for(let i=0;i<json.length;i += MAX_SIZE) {
                let spl = json.substr(i, MAX_SIZE);
                d.push(crypto.publicEncrypt(pubKey, Buffer.from(spl)).toString("hex"));
            }
            data = {"encrypted": d};
        }
        let build = {
            op,
            data,
            seq: helpers.nextSequence(),
            nonce
        }
        let str = JSON.stringify(build);
        let s = trs.sign(str);
        return {s: s.toString("hex"), d: str};
    },
    broadcast(data) {
        trs.sendData(data, globals.getBroadcastAddress());
    },
    sendData(data, addr, encrypt = false) {
        globals.socket.setBroadcast(true);
        let pack = trs.buildPacket(data);
        if(encrypt) {
            let client = globals.known_clients[addr];
            if(client == null || client.x509 == null) return err("Cannot encrypt info to " + addr + " when a connection has not been established.");
            let key =  client.x509.publicKey.toPEM();
            pack = trs.buildPacket(data, {encrypt: true, pubKey: key});
        }
        globals.socket.send(JSON.stringify(pack), 0, JSON.stringify(pack).length, globals.config.port, addr);
        log("Outbound ("+globals.resolveName(addr)+"): " + JSON.stringify(pack));
    }
};
module.exports =trs;