const axios = require('axios');
const https = require('https');
const {err} = require('./debug');

function getNode(serial) {
    const globals = require('./globals');
    let active = globals.activeConnections();
    let node = null;
    Object.keys(active).forEach(e => {
        let o = active[e];
        if(o.x509.serialNumber === serial) node = {info: o, addr: e};
    });
    return node;
}

function appToPubJson(name) {
    const globals = require('./globals');
    let e = globals.apps[name];
    let capp = globals.x509.cert.serialNumber;
    return {
        name: name,
        webhooks: e.webhooks == null ? [] : e.webhooks.map((e) => ({
            id: e.id,
            label: e.label,
            type: e.type,
        })),
        address: capp + '.' + name,
        serviceId: e.serviceId,
        dashAvailable: e.dashUri != null
    };
}

function getApps() {
    const globals = require('./globals');
    return Object.keys(globals.apps).map((f) => {
        return appToPubJson(f);
    });
}

function getAppDash(name) {
    const globals = require('./globals');
    let app = globals.apps[name];
    if(app === undefined) return new Promise((res, rej) => res(null));
    if(app.dashUri == undefined) return new Promise((res, rej) => res(null));
    const agent = new https.Agent({
        rejectUnauthorized: (!app.ignoreSSL)
    });
    return new Promise((res, rej) => {
        axios.get(app.dashUri, {httpsAgent: agent}).then((resp) => {
            res(resp.data);
        }).catch(er => {
            err(er);
            res({error: 100, message: "Error fetching dash."})
        });
    });
}

function triggerAppHook(name, hook, data = {}, source = null) {
    const globals = require('./globals');
    let app = globals.apps[name];
    if(app === undefined) return new Promise((res, rej) => res(null));
    let hk = app.webhooks.find(e => e.id === hook);
    if(hk == null) return new Promise((res, rej) => res(null));
    let src = source != null ? globals.known_clients[source.address] : null;
    let transport = require('./transport');
    const agent = new https.Agent({
        rejectUnauthorized: (!app.ignoreSSL)
    });
    let churl = app.webhooks.find(e => e.type === 'challenge');
    return new Promise((res, rej) => {
        let d = JSON.stringify(data);
        (churl != null ? axios.post(churl.url) : new Promise((res, rej) => res(null))).then(e => {
            let challenge = null;
            if(e != null && e.data.challenge != null && e.data.challenge_id != null) {
                challenge = e.data.challenge_id + ":" + transport.sign(e.data.challenge).toString('hex');
            }
            axios.post(hk.url, d, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Webhook-Origin": (source != null ? "Remote" : "Local"),
                    "X-Source-Serial": (source != null ? src.x509.serialNumber : "local"),
                    "X-Source-IP": (source != null ? source.address : "127.0.0.1"),
                    "X-Node-Signature": transport.sign(d).toString('hex'),
                    "X-Node-Challenge": (challenge != null ? challenge : 'None')
                },
                httpsAgent: agent
            }).then((resp) => {
                res(resp.data);
            }).catch(er => {
                res({error: 100, message: "Error executing webhook. (" + er.message + ")"})
            });
        });
    });
}

function getNodeApps({info, addr}) {
    const globals = require('./globals');
    const transport = require('./transport');
    const {opCode} = require('./enums');
    let nonce = Math.round(Math.random()*50000+1000)
    return new Promise((res, rej) => {
        globals.operations.GENERIC_RESPONSE.responseQueue[nonce] = {
            callback: (data) =>
            {
                res(data);
            },
            addedAt: Date.now(),
        }
        transport.sendData({op: opCode.APPS_LIST_REQUEST, data: null, nonce}, addr);
    });
}

function getNodeServices(srv) {
    const globals = require('./globals');
    const transport = require('./transport');
    const {opCode} = require('./enums');
    let nonce = Math.round(Math.random()*50000+1000)
    return new Promise((res, rej) => {
        let all = {};
        Object.keys(globals.apps).forEach(name => {
            let app = globals.apps[name];
            if(app.serviceId == null) return;
            if(app.serviceId === srv) {
                if(all[globals.x509.cert.serialNumber] == null) all[globals.x509.cert.serialNumber] = [];
                all[globals.x509.cert.serialNumber].push(appToPubJson(name));
            }
        });
        globals.operations.GENERIC_RESPONSE.responseQueue[nonce] = {
            callback: (data, inf) =>
            {
                let node = globals.known_clients[inf.address];
                all[node.x509.serialNumber] = data.apps;
            },
            addedAt: Date.now(),
            expectBatch: true
        }
        transport.broadcast({op: opCode.C_FIND, data: {sid: srv}, nonce});
        setTimeout(() => {
            delete globals.operations.GENERIC_RESPONSE.responseQueue[nonce];
            res(all);
        }, 1000);
    });
}

function getNodeAppDash({info, addr}, name) {
    const globals = require('./globals');
    const transport = require('./transport');
    const {opCode} = require('./enums');
    let nonce = Math.round(Math.random()*50000+1000)
    return new Promise((res, rej) => {
        globals.operations.GENERIC_RESPONSE.responseQueue[nonce] = {
            callback: (data) =>
            {
                res(data);
            },
            addedAt: Date.now(),
        }
        transport.sendData({op: opCode.APP_DASH, data: {name: name}, nonce}, addr);
    });
}

function triggerNodeHook({info, addr}, name, hook, data = {}) {
    const globals = require('./globals');
    const transport = require('./transport');
    const {opCode} = require('./enums');
    let nonce = Math.round(Math.random()*50000+1000)
    return new Promise((res, rej) => {
        globals.operations.GENERIC_RESPONSE.responseQueue[nonce] = {
            callback: (data) =>
            {
                res(data);
            },
            addedAt: Date.now(),
        }
        transport.sendData({op: opCode.TRIGGER_HOOK, data: {name: name, webhook: hook, data: data}, nonce}, addr);
    });
}

module.exports = {getNode, getApps, getAppDash, triggerAppHook, getNodeApps, getNodeAppDash, getNodeServices, triggerNodeHook, appToPubJson};