const fs = require('fs');
const path = require('path');

let storagePath = path.join(__dirname, 'storage');
if(!fs.existsSync(storagePath)) fs.mkdirSync(storagePath);
if(!fs.existsSync(path.join(storagePath, 'known_clients.json'))) fs.writeFileSync(path.join(storagePath, 'known_clients.json'), '{}');
if(!fs.existsSync(path.join(storagePath, 'security.json'))) fs.writeFileSync(path.join(storagePath, 'security.json'), '{}');

const udp = require('dgram');
const {Certificate, PrivateKey} = require('@fidm/x509');
const {sign, verify, broadcast, sendData} = require('./transport');
const rimraf = require('rimraf');
const pem = require('pem');
const os = require('os');
const {opCode} = require('./enums');
const {err, log} = require('./debug');
const crypto = require('crypto');
const {responseQueue} = require('./ops/genericResponse');
const schemas = require('./schemas');
const helpers = require('./helpers');

let globals = require('./globals');

if(!fs.existsSync(path.join(__dirname, "config.json")))
    fs.copyFileSync(path.join(__dirname, "config.example.json"), path.join(__dirname, "config.json"));
globals.config = require('./config.json');

function genCert() {
    if(fs.existsSync(path.join(__dirname, "certs"))) rimraf.sync(path.join(__dirname, "certs"));
    fs.mkdirSync(path.join(__dirname, "certs"));
    console.log("REGENERATING CERTIFICATE");
    pem.createCSR({
        commonName: os.hostname()
    }, (e, {csr, clientKey}) => {
        if(e != null) {
            console.error(e);
            return;
        }
        fs.writeFileSync(path.join(__dirname, "certs", "pkey.pem"), clientKey);
        globals.x509.pkey_text = clientKey;
        globals.x509.pkey = PrivateKey.fromPEM(clientKey);
        pem.createCertificate({
            serviceKey: clientKey,
            selfSigned: true,
            csr,
            days: 365,
            extFile: path.join(__dirname, "v3.conf")
        }, (e, {certificate, csr, clientKey, serviceKey}) => {
            fs.writeFileSync(path.join(__dirname, "certs", "cert.pem"), certificate);
            globals.x509.cert_text = certificate;
            globals.x509.cert = Certificate.fromPEM(certificate);
            startBind();
        })
    });
}

// Create/Load X509
if(fs.existsSync(path.join(__dirname, "certs"))) {
    globals.x509.cert_text = fs.readFileSync(path.join(__dirname, "certs", "cert.pem")).toString();
    globals.x509.cert = Certificate.fromPEM(globals.x509.cert_text);
    globals.x509.pkey_text = fs.readFileSync(path.join(__dirname, "certs", "pkey.pem")).toString();
    globals.x509.pkey = PrivateKey.fromPEM(globals.x509.pkey_text);
    if(globals.x509.cert.validTo < Date.now()) {
        console.log("X509 Certificate expired.")
        genCert();
    }
    let ts = sign("TEST DATA");
    if(!verify("TEST DATA", ts, globals.x509.cert)) {
        console.error("X509 Certificate did not match private key. Expected signature " + ts.toString('hex'));
        genCert();
    } else {
        console.log("X509 Certificate Verified. Signature " + ts.toString('hex'));
        startBind();
    }
} else {
    genCert();
}

function startBind() {
    console.log("HERE WE GO!");
    globals.socket = udp.createSocket('udp4');
    globals.socket.on('error', (e) => {
        console.error('[ERROR] ' + e);
    });
    globals.socket.on('close', (e) => {
        console.error('SOCKET CLOSED');
    });
    globals.socket.on('message', (msg, info) => {
        // Never trust the data on initial read. Always inspect it.
        let js = msg.toString();
        log("Inbound (" + globals.resolveName(info.address) + "): " + js);
        try {
            js = JSON.parse(js);
        } catch(e) {
            return;
        }
        /*

        Basic message format:
        {
            "s": <SIGNATURE STRING>,
            "d": <STRING OF OBJECT: {
                "op": <OPCODE STRING>,
                "data": <MISC DATA>,
                "nonce": <NONCE/NULL>,
                "seq": <SEQUENCE NUMBER>
            }>
        }

         */
        if(!schemas.validate(js, {"$ref": "/pack"}).valid) return err("/pack check fail.");
        let data = null;
        try {
            data = JSON.parse(js.d);
        } catch(e) {
            err("D2 Fail");
            return;
        }
        let dv = schemas.validate(data, {"$ref": "/pack/data"});
        if(!dv.valid) return err("/pack/data check fail. " + dv.errors);
        if(globals.operations[data.op] === undefined) return err("O2 Fail");
        let op = globals.operations[data.op];
        let sign = Buffer.from(js.s, 'hex');
        if(verify(js.d, sign, globals.x509.cert)) return err("SE1 Fail"); // Avoid messages from self
        let addr = info.address;
        if(globals.known_clients[addr] == null) globals.known_clients[addr] = {x509: null, lastContact: Date.now(), pingSent: false};
        let client = globals.known_clients[addr];
        if(op.verifySignature || op.decryptData) {
            if(client.x509 == null) return err("V1 Fail"); // No existing certificate
            if(!verify(js.d, sign, client.x509)) return err("V2 Fail");
            log("Signature from " + info.address + " valid.");
        }
        if(!helpers.verifySequence(addr, data.seq)) return err("SQ2 Fail");
        client.seq = data.seq;
        client.lastContact = Date.now();
        client.pingSent = false;
        log("Processing " + data.op + " from " + info.address);
        if(op.decryptData) {
            if(data.data.encrypted === undefined) return;
            try {
                let decr = "";
                if(!Array.isArray(data.data.encrypted)) return err("Not Array");
                let ind = 0;
                data.data.encrypted.forEach(e => {
                    decr += crypto.privateDecrypt(globals.x509.pkey_text, Buffer.from(e, 'hex')).toString();
                    ind++;
                });
                op.invoke(JSON.parse(decr), info, data.nonce);
            } catch(e) {
                return err(e);
            }
        } else
        {
            op.invoke(data.data, info, data.nonce);
        }
        log("DONE");
    });


    globals.socket.bind(globals.config.port, globals.getUDPBindAddress(), () => {
        console.log("Sending DISCOVER");
        broadcast({op: opCode.DISCOVER, data: null});
        if(globals.config.discover != null) {
            globals.config.discover.forEach(ip => {
                sendData({op: opCode.DISCOVER, data: null}, ip);
            });
        }
    });
    require('./webserver/webserver');

    setInterval(() => {
        let stale = globals.staleConnections(60000*3);
        let queue = globals.operations.PONG.pongQueue;
        Object.keys(stale).forEach(e => {
            let client = stale[e];
            let nonce = Math.round(Math.random()*50000+1000);
            queue[nonce] = () => {
                log("Connection with " + e + " reconfirmed.");
            };
            client.pingSent = true;
            sendData({op: opCode.PING, data: null, nonce: nonce}, e);
            log("Sent ping to stale client " + e);
        });
    }, 30000); // Connections Handler
}

setInterval(() => {
    Object.keys(globals.operations.GENERIC_RESPONSE.responseQueue).forEach(nonce => {
        let obj = globals.operations.GENERIC_RESPONSE.responseQueue[nonce];
        if(obj.addedAt > Date.now()-5000) return;
        obj.callback({error: 0, message: "Response timeout of 5s exceeded."});
        delete responseQueue[nonce];
    });
}, 1000);
