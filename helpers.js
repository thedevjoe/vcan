const globals = require('./globals');
let known = require('./storage/known_clients.json');
let security = require('./storage/security.json');
const fs = require('fs');
const path = require('path');

function getKnown(addr) {
    if(known[addr] == null) known[addr] = {pkey: null, sequence: -1};
    return known[addr];
}

function registerNode(addr, x509) {
    let cl = globals.known_clients[addr];
    if(cl.x509 != null) return true;
    let raw = x509.publicKeyRaw.toString('base64');
    let k = getKnown(addr);
    if(k.pkey != null && k.pkey !== raw) {
        console.error('WARNING: Stored public key does not match public key provided. Client ' + addr + ' may have been compromised. Refusing to accept x509.');
        return false;
    }
    k.pkey = raw;
    globals.known_clients[addr].x509 = x509;
    saveKnown();
    return true;
}

function verifySequence(addr, seq) {
    let k = getKnown(addr);
    if(k.sequence >= seq) {
        return false;
    }
    k.sequence = seq;
    saveKnown();
    return true;
}

function saveKnown() {
    fs.writeFileSync(path.join(__dirname, 'storage', 'known_clients.json'), JSON.stringify(known));
}

function nextSequence() {
    let seq = null;
    if(security.sequence == null) seq = security.sequence = 0;
    else seq = security.sequence++;
    saveSecurity();
    return seq;
}

function saveSecurity() {
    fs.writeFileSync(path.join(__dirname, 'storage', 'security.json'), JSON.stringify(security));
}

module.exports = {registerNode, verifySequence, nextSequence};